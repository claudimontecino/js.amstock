from fanstatic import Library, Resource, get_needed


library = Library('amstock', 'resources')

amstock_css = Resource(library, 'style.css')
amstock = Resource(library, 'amstock.js',
                   depends=[amstock_css])


def find_images_url():
    needed = get_needed()
    library_url = needed.library_url(library)
    return '%s/images/' % library_url


